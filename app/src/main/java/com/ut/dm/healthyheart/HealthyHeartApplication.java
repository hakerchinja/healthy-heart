package com.ut.dm.healthyheart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthyHeartApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthyHeartApplication.class, args);
	}

}
