The data understanding phase includes four tasks. These are:

Gathering data

Describing data

Exploring data

Verifying data quality 


1) Gathering data
Here we need to verify that we have acquired the data, or at least gained access to the data, tested the data access process, and verified that the data exists. Afterwards, we will need to load the data into any tools which are going to be used for data mining to verify that the tools are compatible with the data.

a) So, first we will outline the data requirements. List of types of data necessary to address the data mining goals. 
The data contains 76 attributes. The goal field refers to the presence of heart disease in the patient. The data mining goals would be building a classifier/classifiers which can predict for a given input of data whether the patient has heart disease or not, using multilinear regression for using the attributes which really influence on the prediction of heart disease presence, seperating the data in training data and test data, visualization of the data. For this, 4 datasets will be used. All of them are of type multivariate and the area of the datasets is Life. 
Attribute characteristics are either:

Categorical,

Integer or

Real

Each database has the same instance format. The data format of all of the 4 datasets is space seperated values using multiple new lines.

b) Verify data availability

Here we need to confirm that the data really exists, and it can be used. The data exists and it can be downloaded for usage from the following web link: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/.
The four datasets for heart diseases diagnosis in patients are from:

Cleveland Clinic Foundation (cleveland.data)

Hungarian Institute of Cardiology, Budapest (hungarian.data)

V.A. Medical Center, Long Beach, CA (long-beach-va.data)

University Hospital, Zurich, Switzerland (switzerland.data)

There is a need either for substituting the data source with alternative data source for Clevland Clinic Foundation because the data is corrupted, or to perform cleaning of the data from bad values. There is also a warning about this case specified by the donors of the data. The donors of the data provide a light dataset with only 14 attributes. In our opinion this will be enough for recreating the data, because only some of the rows consist bad data. The other 3 databases are good.

There is no need for narrowing the scope of the project, because the data itself will be enough for diagnosing heart diseases. 

With gathering new data, if possible, the outcome of the project could only possibly benefit in accuracy, but that is questionable.

c) Define selection criteria.

The specific data sources are the following:

Cleveland Clinic Foundation: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/cleveland.data

Hungarian Institute of Cardiology, Budapest: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/hungarian.data

V.A. Medical Center, Long Beach, CA: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/long-beach-va.data

University Hospital, Zurich, Switzerland: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/switzerland.data

explanation of the attribute names: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/heart-disease.names

warrning about clevland data: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/WARNING

lightweight clevland data: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.cleveland.data

The databases contains 76 attributes. The "goal" field refers to the presence of heart disease in the patient.  It is integer valued from 0 (no presence) to 4. The experiments will be concentrated on simply attempting to distinguish presence (values 1,2,3,4) from absence (value 0). The most important fields from the databases for inferring whether there is heart disease in the patient are:

age, sex, cp, trestbps, chol, fbs, restecg, thalach, exang, oldpeak, slope, ca, thal, num

All of the other attributes can be found on the following link: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/heart-disease.names

d) Next step is to try to import the data. As we already specified, the data format is space separated value with multiple lines. Because of this, first we need to structure the data. For importing the data we will use R studio. But before we use R, we will use Java to prepare the data file structure from space separated with multiple lines to csv single lines. We will use Java due to R's inability to read the data formats of the sources.


Now, as it can be seen from the previous part of the report, the different datasets are gathered from the specified sources and therefore I can confirm that I have obtained the data, which is compatibile with R's data-mining platform and will be enough for the goal which is defined for this project. The work that was needed here was challenging and a bit frustrating. First, it was frustrating because of the corrupted data on one of the data sets. Second, it was frustrating because the data format was not appropriate and I needed to make a change. Third, it was diffucult because the data fileds did not have headers, and headers needed to be imported.

2. Describing data

Now a general description will follow.

First the source of the data. The source of the data is from the machine learning repository. The repo can be found on the following link: http://archive.ics.uci.edu/ml/index.php. This repository is a collection of databases, domain theories, and data generators that are used by the machine learning community for the empirical analysis of machine learning algorithms. It is widely used by students, educators and researchers all over the world as a primary source of the machine learning data sets.

Second the format of the data. As I disscused previously, the format of the data is space separated multi line format. Therefore I implied changes, in order to get the format to CSV which was readable for R.

Third the number of cases. There is only one case related to diagnosis of heart diseases.

Fourth the number of descriptions. There are two descriptions of the databases. They can be found on the following links:
http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/heart-disease.names and
http://archive.ics.uci.edu/ml/datasets/Heart+Disease

Fifth, an evaluation of the databases in aspect of suitability of the data for the data mining goals.
The databases contain 76 attributes. The "goal" field refers to the presence of heart disease in the patient.  It is integer valued from 0 (no presence) to 4. The experiments will be concentrated on simply attempting to distinguish presence (values 1,2,3,4) from absence (value 0). The most important fields from the databases for inferring whether there is heart disease in the patient are:

age - age in years

sex - male(1), female(0)

cp - chest pain type
        
        -- Value 1: typical angina
        
        -- Value 2: atypical angina
        
        -- Value 3: non-anginal pain
        
        -- Value 4: asymptomatic
        
trestbps: resting blood pressure (in mm Hg on admission to the hospital)
        
chol: serum cholestoral in mg/dl

fbs: (fasting blood sugar > 120 mg/dl)  (1 = true; 0 = false)

restecg: resting electrocardiographic results
       
        -- Value 0: normal
       
        -- Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
       
        -- Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria

thalach: maximum heart rate achieved

exang: exercise induced angina (1 = yes; 0 = no)

oldpeak: ST depression induced by exercise relative to rest

slope: the slope of the peak exercise ST segment
        
        -- Value 1: upsloping
        
        -- Value 2: flat
        
        -- Value 3: downsloping, 
        
ca: number of major vessels (0-3) colored by flourosopy

thal:  3 = normal; 6 = fixed defect; 7 = reversable defect

num: diagnosis of heart disease (angiographic disease status)
        
        -- Value 0: < 50% diameter narrowing
        
        -- Value 1: > 50% diameter narrowing
        
        (in any major vessel: attributes 59 through 68 are vessels)
        
All of the other attributes can be found on the following link: http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/heart-disease.names

   
3. Exploring data

This directory contains 4 databases concerning heart disease diagnosis.

All attributes are numeric-valued. The data was collected from the four following locations:

     1. Cleveland Clinic Foundation (cleveland.data)
     2. Hungarian Institute of Cardiology, Budapest (hungarian.data)
     3. V.A. Medical Center, Long Beach, CA (long-beach-va.data)
     4. University Hospital, Zurich, Switzerland (switzerland.data)

Each database has the same instance format. While the databases have 76 raw attributes, only 14 of them are actually used.
   
The names and social security numbers of the patients were removed from the database, replaced with dummy values.

One file has been "processed", that one containing the Cleveland database. All four unprocessed files also exist in this directory.

Number of Instances: 

Database: # of instances:

Cleveland: 303

Hungarian: 294

Switzerland: 123

Long Beach VA: 200

Attributes:

1 id: patient identification number 

2 ccf: social security number (I replaced this with a dummy value of 0) 

3 age: age in years 

4 sex: sex (1 = male; 0 = female) 

5 painloc: chest pain location (1 = substernal; 0 = otherwise) 

6 painexer (1 = provoked by exertion; 0 = otherwise) 

7 relrest (1 = relieved after rest; 0 = otherwise) 

8 pncaden (sum of 5, 6, and 7) 

9 cp: chest pain type 

    -- Value 1: typical angina 

    -- Value 2: atypical angina 

    -- Value 3: non-anginal pain 

    -- Value 4: asymptomatic 

10 trestbps: resting blood pressure (in mm Hg on admission to the hospital) 

11 htn 

12 chol: serum cholestoral in mg/dl 

13 smoke: I believe this is 1 = yes; 0 = no (is or is not a smoker) 

14 cigs (cigarettes per day) 

15 years (number of years as a smoker) 

16 fbs: (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false) 

17 dm (1 = history of diabetes; 0 = no such history) 

18 famhist: family history of coronary artery disease (1 = yes; 0 = no) 

19 restecg: resting electrocardiographic results 

    -- Value 0: normal 

    -- Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV) 

    -- Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria 

20 ekgmo (month of exercise ECG reading) 

21 ekgday(day of exercise ECG reading) 

22 ekgyr (year of exercise ECG reading) 

23 dig (digitalis used furing exercise ECG: 1 = yes; 0 = no) 

24 prop (Beta blocker used during exercise ECG: 1 = yes; 0 = no) 

25 nitr (nitrates used during exercise ECG: 1 = yes; 0 = no) 

26 pro (calcium channel blocker used during exercise ECG: 1 = yes; 0 = no) 

27 diuretic (diuretic used used during exercise ECG: 1 = yes; 0 = no) 

28 proto: exercise protocol 

    1 = Bruce 

    2 = Kottus 

    3 = McHenry 

    4 = fast Balke 

    5 = Balke 

    6 = Noughton 

    7 = bike 150 kpa min/min (Not sure if "kpa min/min" is what was written!) 

    8 = bike 125 kpa min/min 

    9 = bike 100 kpa min/min 

    10 = bike 75 kpa min/min 

    11 = bike 50 kpa min/min 

    12 = arm ergometer 

29 thaldur: duration of exercise test in minutes 

30 thaltime: time when ST measure depression was noted 

31 met: mets achieved 

32 thalach: maximum heart rate achieved 

33 thalrest: resting heart rate 

34 tpeakbps: peak exercise blood pressure (first of 2 parts) 

35 tpeakbpd: peak exercise blood pressure (second of 2 parts) 

36 dummy 

37 trestbpd: resting blood pressure 

38 exang: exercise induced angina (1 = yes; 0 = no) 

39 xhypo: (1 = yes; 0 = no) 

40 oldpeak = ST depression induced by exercise relative to rest 

41 slope: the slope of the peak exercise ST segment 

    -- Value 1: upsloping 

    -- Value 2: flat 

    -- Value 3: downsloping 

42 rldv5: height at rest 

43 rldv5e: height at peak exercise 

44 ca: number of major vessels (0-3) colored by flourosopy 

45 restckm: irrelevant 

46 exerckm: irrelevant 

47 restef: rest raidonuclid (sp?) ejection fraction 

48 restwm: rest wall (sp?) motion abnormality 
    
    0 = none 
    
    1 = mild or moderate 

    2 = moderate or severe 

    3 = akinesis or dyskmem (sp?) 

49 exeref: exercise radinalid (sp?) ejection fraction 

50 exerwm: exercise wall (sp?) motion 

51 thal: 3 = normal; 6 = fixed defect; 7 = reversable defect 

52 thalsev: not used 

53 thalpul: not used 

54 earlobe: not used 

55 cmo: month of cardiac cath (sp?) (perhaps "call") 

56 cday: day of cardiac cath (sp?) 

57 cyr: year of cardiac cath (sp?) 

58 num: diagnosis of heart disease (angiographic disease status) 

    -- Value 0: < 50% diameter narrowing 

    -- Value 1: > 50% diameter narrowing 

(in any major vessel: attributes 59 through 68 are vessels) 

59 lmt 

60 ladprox 

61 laddist 

62 diag 

63 cxmain 

64 ramus 

65 om1 

66 om2 

67 rcaprox 

68 rcadist 

69 lvx1: not used 

70 lvx2: not used 

71 lvx3: not used 

72 lvx4: not used 

73 lvf: not used 

74 cathef: not used 

75 junk: not used 

76 name: last name of patient (I replaced this with the dummy string "name")

Missing Attribute Values: Several.  Distinguished with value -9.0.

Class Distribution:
    
        Database:      0   1   2   3   4 Total
    
          Cleveland: 164  55  36  35  13   303
    
          Hungarian: 188  37  26  28  15   294
    
        Switzerland:   8  48  32  30   5   123
    
      Long Beach VA:  51  56  41  42  10   200
      
4. Verifying data quality

The data which we have is good enough to support our goals. There are several problems, such as bad values, and missing values, but those are solvable because this exist only on one of the four datasets. Also every dataset has its own backup lightweight dataset which can be used for bad values and missing values. The data we need for the defined goal exists. Also the data we need is available and does not pose any usage requirements. And the data has incorrect values, but it does not have severe quality issues, therefore the quality of the data is on a "more than enough" level for the defined goal.

