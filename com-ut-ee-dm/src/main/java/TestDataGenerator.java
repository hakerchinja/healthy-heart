import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TestDataGenerator {

    public static final int PEOPLE_PER_CYCLE = 400;

    public static void main(String[] args){
        List<Person> personList = generateData();
        Helper.exportTestDataToFile("data/testdata.csv", personList);
    }

    private static List<Person> generateData(){
        List<Person> allPeople = Helper.readPeopleFromFile("data/data-combined.csv");
        List<Country> countries = Helper.readCountriesFromFile("data/world-happiness-report.csv");
        List<Person> personList = new ArrayList<>();

        int id = 1;

        for(Country country : countries){
            double happiness = country.getHappiness();
            if(country.getId() == 4){
                //switzerland
                List<Person> people = getPeopleForCountry("switzerland", allPeople);

                for(Person person : people){
                    Person newPerson = createPerson(person, id, country.getId(), happiness);
                    personList.add(newPerson);
                    id++;
                }

            } else if(country.getId() == 14){
                //United States
                //long-beach-va + cleveland
                List<Person> people = getPeopleForCountry("long-beach-va", allPeople);
                people.addAll(getPeopleForCountry("cleveland", allPeople));

                for(Person person : people){
                    Person newPerson = createPerson(person, id, country.getId(), happiness);
                    personList.add(newPerson);
                    id++;
                }

            } else if(country.getId() == 75){
                //hungary
                List<Person> people = getPeopleForCountry("hungary", allPeople);

                for(Person person : people){
                    Person newPerson = createPerson(person, id, country.getId(), happiness);
                    personList.add(newPerson);
                    id++;
                }
            } else {
                for(int i=0;i<PEOPLE_PER_CYCLE;i++){
                    int randomIndex = Helper.generateRandomInt(0, allPeople.size() - 1);
                    Person person = allPeople.get(randomIndex);
                    Person newPerson = createPerson(person, id, country.getId(), happiness);
                    personList.add(newPerson);
                    id++;
                }
            }
        }

        return personList;
    }

    private static Person createPerson(Person person, int id, int country, double happiness){
        Person newPerson = new Person();
        newPerson.setId(id);
        newPerson.setCountry(country);
        newPerson.setHappiness(happiness);
        newPerson.setAge(person.getAge());
        newPerson.setSex(person.getSex());
        newPerson.setCp(person.getCp());
        newPerson.setTrestbps(person.getTrestbps());
        newPerson.setChol(person.getChol());
        newPerson.setFbs(person.getFbs());
        newPerson.setRestecg(person.getRestecg());
        newPerson.setThalach(person.getThalach());
        newPerson.setExang(person.getExang());
        newPerson.setOldpeak(person.getOldpeak());
        newPerson.setSlope(person.getSlope());
        newPerson.setNum(person.getNum());
        return newPerson;
    }

    private static Person generateRandomPerson(long id){
        Person person = new Person();
        person.setId(id);
        person.setSex(Helper.generateRandomInt(0, 1));
        person.setCp(Helper.generateRandomInt(1, 4));
        person.setTrestbps(Helper.generateRandomInt(80, 200));
        person.setChol(Helper.generateRandomInt(85, 600));
        person.setFbs(Helper.generateRandomInt(0, 1));
        person.setRestecg(Helper.generateRandomInt(0, 2));
        person.setThalach(Helper.generateRandomInt(60, 200));
        person.setExang(Helper.generateRandomInt(0, 1));
        person.setSlope(Helper.generateRandomInt(0, 3));
        person.setNum(Helper.generateRandomInt(0, 4));

        int temp = Helper.generateRandomInt(0, 3);
        double val = 0;
        if(temp == 0){
            val = Helper.generateRandomDouble(0, 2.6);
            val *= -1;
        } else {
            val = Helper.generateRandomDouble(0, 6.2);
        }
        person.setOldpeak(val);

        int flag = Helper.generateRandomInt(0, 100);
        if(flag == 10){
            person.setAge(Helper.generateRandomInt(18, 30));
        } else {
            person.setAge(Helper.generateRandomInt(31, 80));
        }

        return person;
    }

    private static List<Person> getPeopleForCountry(String countryName, List<Person> allPeople){
        List<Person> peopleForCountry = new ArrayList<>();
        for(Person person : allPeople){
            if(person.getCountryName().equals(countryName)){
                peopleForCountry.add(person);
            }
        }
        return peopleForCountry;
    }
}

class Person{
    private long id;
    private int age; //values from 18 to 80
    private int sex; //1-male; 0-female
    private int cp; //1, 2, 3, 4
    private int trestbps; //values from 80 to 200
    private int chol; //values from 85 to 603
    private int fbs; //0, 1
    private int restecg; //0,1,2
    private int thalach; //values from 60 to 202
    private int exang; //0,1
    private double oldpeak; //from -2.6 to 6.2
    private int slope; //0,1,2,3
    private int num; //0,1,2,3,4
    private int country;
    private double happiness;
    private String countryName;

    public Person(){

    }

    public Person(long id, int age, int sex, int cp, int trestbps, int chol, int fbs, int restecg, int thalach, int exang, double oldpeak, int slope, int num, double happiness) {
        this.id = id;
        this.age = age;
        this.sex = sex;
        this.cp = cp;
        this.trestbps = trestbps;
        this.chol = chol;
        this.fbs = fbs;
        this.restecg = restecg;
        this.thalach = thalach;
        this.exang = exang;
        this.oldpeak = oldpeak;
        this.slope = slope;
        this.num = num;
        this.happiness = happiness;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public int getTrestbps() {
        return trestbps;
    }

    public void setTrestbps(int trestbps) {
        this.trestbps = trestbps;
    }

    public int getChol() {
        return chol;
    }

    public void setChol(int chol) {
        this.chol = chol;
    }

    public int getFbs() {
        return fbs;
    }

    public void setFbs(int fbs) {
        this.fbs = fbs;
    }

    public int getRestecg() {
        return restecg;
    }

    public void setRestecg(int restecg) {
        this.restecg = restecg;
    }

    public int getThalach() {
        return thalach;
    }

    public void setThalach(int thalach) {
        this.thalach = thalach;
    }

    public int getExang() {
        return exang;
    }

    public void setExang(int exang) {
        this.exang = exang;
    }

    public double getOldpeak() {
        return oldpeak;
    }

    public void setOldpeak(double oldpeak) {
        this.oldpeak = oldpeak;
    }

    public int getSlope() {
        return slope;
    }

    public void setSlope(int slope) {
        this.slope = slope;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getHappiness() {
        return happiness;
    }

    public void setHappiness(double happiness) {
        this.happiness = happiness;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String toString(){
        return id + "," + age + "," + sex + "," + cp + "," + trestbps + "," + chol + "," + fbs + "," + restecg + "," + thalach + "," + exang + "," + oldpeak + "," + slope + "," + num + "," + country +  ","+ happiness;
    }

    public static String getHeader(){
        return "id,age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,num,country,happiness";
    }
}

class Country{
    private int id;
    private double happiness;

    public Country(){

    }

    public Country(int id, double happiness){
        this.id = id;
        this.happiness = happiness;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getHappiness() {
        return happiness;
    }

    public void setHappiness(double happiness) {
        this.happiness = happiness;
    }

    public String toString(){
        return id + "," + happiness;
    }
}

class Helper{

    public static List<Person> readPeopleFromFile(String fileName){
        List<Person> people = new ArrayList<>();
        BufferedReader br = null;
        String currLine = "";

        try{
            br = new BufferedReader(new FileReader(fileName));
            boolean isFirstLine = true;
            while ((currLine = br.readLine()) != null) {
                if(!isFirstLine){
                    currLine = currLine.trim();
                    String[] info = currLine.split(",");
                    Person person = new Person();
                    person.setId(Integer.parseInt(info[0]));
                    person.setAge(Integer.parseInt(info[2]));
                    person.setSex(Integer.parseInt(info[3]));
                    person.setCp(Integer.parseInt(info[8]));
                    person.setTrestbps(Integer.parseInt(info[9]));
                    person.setChol(Integer.parseInt(info[11]));
                    person.setFbs(Integer.parseInt(info[15]));
                    person.setRestecg(Integer.parseInt(info[18]));
                    person.setThalach(Integer.parseInt(info[31]));
                    person.setExang(Integer.parseInt(info[37]));
                    person.setOldpeak(Double.parseDouble(info[39]));
                    person.setSlope(Integer.parseInt(info[40]));
                    person.setNum(Integer.parseInt(info[57]));
                    person.setCountryName(info[81]);

                    people.add(person);
                }
                isFirstLine = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return people;
    }

    public static List<Country> readCountriesFromFile(String fileName){
        List<Country> countries = new ArrayList<>();
        BufferedReader br = null;
        String currLine = "";

        try{
            br = new BufferedReader(new FileReader(fileName));
            boolean isFirstLine = true;
            while ((currLine = br.readLine()) != null) {
                if(!isFirstLine){
                    currLine = currLine.trim();
                    String[] info = currLine.split(",");
                    int id = Integer.parseInt(info[1]);
                    double happiness = Double.parseDouble(info[2]);
                    Country country = new Country(id, happiness);
                    countries.add(country);
                }
                isFirstLine = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return countries;
    }

    public static void exportTestDataToFile(String fileName, List<Person> personList){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(Person.getHeader() + "\n");

            for(Person person : personList){
                writer.write(person.toString() + "\n");
            }

        } catch (IOException e) {
            System.err.println(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    public static int generateRandomInt(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static double generateRandomDouble(double min, double max){
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

}