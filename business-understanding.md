## Business understanding

For our project, we're going to use a dataset consisted of patient information including the presence of heart disease in the patient, which can be found here: http://archive.ics.uci.edu/ml/datasets/Heart+Disease.

1. Identifying the business goals

    a. *Background*: This project intends to benefit the general public. Based on the publicly available data for patients that either have or don't have heart disease, we would like to create a prediction model that outputs the probability of a person having a heart disease based on personal information about that person. We would also like to see which attributes contribute positively or negatively to having a heart disease and outline them. Finally, we would like to present these results with interesting visualizations in order to inform the general public about the risk factors that contribute to heart disease.
    
    b. *Goal*: 
    
        - identify the biggest risk factors that contribute to heart disease
        
        - test whether the country where people live (some economic or financial factors) influences the rate of heart disease.
        
        - inform (and warn) the general public about the risks in order to try to reduce the heart disease rate in people.
        
    c. *Business success criteria*: The results will be measured using the following measures:
    
        - accuracy of the classifier on test data (the classifier will be used to predict the chance of a person getting a heart disease based on personal information such as age, whether the person smokes etc.)
        
        - the confidence for the importance of each attributes (which attributes contribute to whether the person is likely or not to have a heart disease)
    
2. Assessing the situation

    a. *Inventory of resources*:
    
        - people: Simona Micevska (data mining and data visualization) and Hristijan Sardjoski (data mining and data visualization)
        
        - hardware: 2 x laptop HP elitebook 840 g4.
        
        - software: Windows 10 operating system, R, RStudio, Python, Jupyter, Javascript, D3.js, WebStorm
        
        - data: http://archive.ics.uci.edu/ml/datasets/Heart+Disease
        
    b. *Requirements, assumptions, and constraints*:
    
        - requirements: the only requirement is that this project is done on time before the poster session. We have access to the data and all other resources are available.
        
    c. *Risks and contingencies*:
    
        - power/internet outage: if this situation arises in the days when we'll work on the project, we would have to move our "office" to an internet cafe or the computer science institute's rooms.
        
    d. *Terminology*:
    
        - classifier: "An algorithm that implements classification, especially in a concrete implementation, is known as a classifier. The term "classifier" sometimes also refers to the mathematical function, implemented by a classification algorithm, that maps input data to a category.
        
    
    e. *Costs and benefits:*
    
        - not applicable

3. Defining the data-mining goals

    a. *Goal 1: Clean the dataset*
    
        - success criteria: visual inspection by Hristijan Sardjoski

    b. *Goal 2: Build a prediction model based on the data*
        
        - success criteria: accuracy of the model based on test data (our aim would be more than 90% accuracy)
        
    c. *Goal 3: Identify which attributes contribute more to the heart disease, and whether they have a positive or a negative effect.*
    
        - success criteria: confidence measure for each of these attributes.
        
    d. *Goal 4: Identify whether the country where a person live has an effect of the possibility for a heart disease*
    
        - success criteria: p-value