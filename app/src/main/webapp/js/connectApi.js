(function () {
    function getCountries(callback) {
        $.ajax({
            url:"/api/countries",
            type:"GET",
            contentType:"charset=utf-8",
            dataType:"json",
            success: function(data) {
                if(callback){
                    callback(data);
                }
            }
        })
    };

    function getPeople(callback) {
        $.ajax({
            url:"/api/people",
            type:"GET",
            contentType:"charset=utf-8",
            dataType:"json",
            success: function(data) {
                if(callback){
                    callback(data);
                }
            }
        })
    };


    ConnectApi = {getPeople: getPeople, getCountries: getCountries};
})();

