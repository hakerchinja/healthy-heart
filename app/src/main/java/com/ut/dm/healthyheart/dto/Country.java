package com.ut.dm.healthyheart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Country {
    @JsonProperty("countryName")
    public String countryName;

    @JsonProperty("id")
    public int id;

    @JsonProperty("happiness")
    public double happiness;

    @JsonProperty("whiskerHigh")
    public double whiskerHigh;

    @JsonProperty("whiskerLow")
    public double whiskerLow;

    @JsonProperty("economy")
    public double economy;

    @JsonProperty("family")
    public double family;

    @JsonProperty("health")
    public double health;

    @JsonProperty("freedom")
    public double freedom;

    @JsonProperty("generosity")
    public double generosity;

    @JsonProperty("trustInGovernment")
    public double trustInGovernment;

    @JsonProperty("dystopia")
    public double dystopia;

    @JsonProperty("totalRisk")
    public double totalRisk;

    @JsonProperty("totalRiskMale")
    public double totalRiskMale;

    @JsonProperty("totalRiskFemale")
    public double totalRiskFemale;

    @JsonProperty("totalRiskAgeGroup1") //18-30
    public double totalRiskAgeGroup1;

    @JsonProperty("totalRiskAgeGroup2") //30-40
    public double totalRiskAgeGroup2;

    @JsonProperty("totalRiskAgeGroup3") //40-50
    public double totalRiskAgeGroup3;

    @JsonProperty("totalRiskAgeGroup4") //50-60
    public double totalRiskAgeGroup4;

    @JsonProperty("totalRiskAgeGroup5") //60-70
    public double totalRiskAgeGroup5;

    @JsonProperty("totalRiskAgeGroup6") //70-80
    public double totalRiskAgeGroup6;

    @JsonProperty("avgCp")
    public double avgCp;

    @JsonProperty("avgTrestbps")
    public double avgTrestbps;

    @JsonProperty("avgChol")
    public double avgChol;

    @JsonProperty("avgFbs")
    public double avgFbs;

    @JsonProperty("avgRestecg")
    public double avgRestecg;

    @JsonProperty("avgThalach")
    public double avgThalach;

    @JsonProperty("avgExang")
    public double avgExang;

    @JsonProperty("avgOldpeak")
    public double avgOldpeak;

    @JsonProperty("avgSlope")
    public double avgSlope;

    @JsonProperty("avgNum")
    public double avgNum;
}
