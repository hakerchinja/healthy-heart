var ID = {
    container: "#container",
    selectedCountry: "#selectedCountry",
    totalRisk: "#totalRisk",
    changeToGlobal: "#changeToGlobal",
    comparedToGlobal: "#comparedToGlobal"
};

var COLORS = {
    watermelon: "#FF3B3F",
    sky: "#CAEBF2"
};

var main = (function(){
    var countries, people;

    var init = function(c, p){
      countries = c;
      people = p;

      initButtons();

      map.init(ID.container, countries);
    };

    var initButtons = function(){
        $(ID.changeToGlobal).click(function(){
            specificCountry("Global");
        });
    };

    return {
        init: init
    };

})();

var map = (function(){
    var divContainer;
    var width, height, margin;
    var data;

    var init = function(dc, d){
        divContainer = dc;
        width = $(divContainer).width();
        height = $(divContainer).height();
        data = d;

        margin = {top: 30, bottom: 30, left: 30, right: 30};

        google.charts.load('current', { 'packages':['geochart'], 'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'});
        google.charts.setOnLoadCallback(draw);
    };

    var draw = function(){
        var temp = transformData();
        var chartData = google.visualization.arrayToDataTable(temp);
        var options = {colorAxis: {colors: ['#EFEFEF', '#FF3B3F']}};

        var chart = new google.visualization.GeoChart(document.getElementById('container'));
        chart.draw(chartData, options);

        specificCountry("Global");

        google.visualization.events.addListener(chart, 'select', function () {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                var value = chartData.getValue(selectedItem.row, 0);
                specificCountry(value);
            } else {
                console.info("No data available");
            }
        });
    };

    var transformData = function(){
        var arr = [];
        arr.push(["Country", "Total risk"]);
        for(var i=0;i<data.length;i++){
            temp = [];
            temp.push(data[i].countryName);
            temp.push(data[i].totalRisk*100);
            arr.push(temp);
        }
        return arr;
    };

    return {
        init: init
    };

})();

var scatterChart = (function(){

    var initialized = false;
    var countries;

    var init = function(c){
        countries = c;
        draw();
    };

    var draw = function(){
        if(!initialized){
            initialized = true;
            var data = google.visualization.arrayToDataTable(getData());

            var options = {
                title: 'Happiness vs. Total risk of heart disease',
                hAxis: {title: 'Happiness', minValue: 0},
                vAxis: {title: 'Total risk', minValue: 0},
                legend: 'none'
            };

            var chart = new google.visualization.ScatterChart(document.getElementById('scatterChart'));

            chart.draw(data, options);
        }
    };

    var getData = function(){
        var arr = [];
        arr.push(["Happiness", "Total risk", { role: 'style' }]);

        for(var i=0;i<countries.length;i++){
            var temp = [];
            temp.push(countries[i].happiness);
            temp.push(countries[i].totalRisk);
            temp.push(COLORS.sky);
            arr.push(temp);
        }

        return arr;
    };

    return {
        init: init
    }

})();

$(document).ready(function(){
    ConnectApi.getCountries(function(data){
        window.data = data;
       main.init(data, []);
    });
});

function specificCountry (country) {
    google.charts.load("current", {packages:["corechart"], 'callback': function(){ drawCharts(country) }});
}

function drawCharts(c) {
    scatterChart.init(window.data);

    var country = getPropertiesForCountry(c);

    var f = d3.format(".4f");
    var smallF = d3.format(".2f");

    $(ID.selectedCountry).text(country.countryName);
    $(ID.totalRisk).text(parseFloat(smallF(country.totalRisk*100)) + "%");

    if(country.countryName === "Global"){
        $(ID.comparedToGlobal).hide();
    } else {
        var global = getPropertiesForCountry("Global");
        var sign = global.totalRisk > country.totalRisk ? "-" : global.totalRisk < country.totalRisk ? "+" : "";
        var value = Math.abs(global.totalRisk - country.totalRisk);
        $(ID.comparedToGlobal).text("(" + sign + f(value*100) + " compared to global)");

        var className = "";
        if(sign === "+"){
            className = "over";
        } else if(sign === "-"){
            className = "under";
        } else {
            className = "same";
        }

        $(ID.comparedToGlobal).removeClass();
        $(ID.comparedToGlobal).addClass(className);

        $(ID.comparedToGlobal).show();
    }

    drawFirstChart(country);
    drawSecondChart(country);
    drawThirdChart(country);
}

function drawFirstChart(country) {
    document.getElementById("BarChart1").innerHTML = "";
    var data = google.visualization.arrayToDataTable([
        ['Gender', 'Risk', { role: 'style' }],
        ['Male', country.totalRiskMale*100, COLORS.sky],
        ['Female', country.totalRiskFemale*100, COLORS.watermelon]
    ]);

    var options = {
        title: country.countryName + " Male/Female overall risk factor",
        width: 430,
        height: 170,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    }

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        { calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation" },
        2]);


    var chart = new google.visualization.BarChart(document.getElementById("BarChart1"));
    chart.draw(view, options);
}

function drawSecondChart(country) {

    document.getElementById("BarChart2").innerHTML = "";
    var data = google.visualization.arrayToDataTable([
        ['Age Group', 'Risk', { role: 'style' }],
        ['18-30', country.totalRiskAgeGroup1*100, COLORS.sky],
        ['30-40', country.totalRiskAgeGroup2*100, COLORS.watermelon],
        ['40-50', country.totalRiskAgeGroup3*100, COLORS.sky],
        ['50-60', country.totalRiskAgeGroup4*100, COLORS.watermelon ],
        ['60-70', country.totalRiskAgeGroup5*100, COLORS.sky],
        ['over 70', country.totalRiskAgeGroup6*100, COLORS.watermelon]
    ]);

    var options = {
        title: country.countryName + " Age groups risk factor",
        width: 430,
        height: 170,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    }

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        { calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation" },
        2]);

    var chart2 = new google.visualization.BarChart(document.getElementById('BarChart2'));
    chart2.draw(view, options);

    var chart3 = new google.visualization.BarChart(document.getElementById('BarChart3'));
    chart3.draw(view, options);
}

function drawThirdChart(country) {

    document.getElementById("BarChart3").innerHTML = "";

    var data = google.visualization.arrayToDataTable([
        ['Indicating factors', 'Value', { role: 'style' }],
        ['Cholesterol', country.avgChol, COLORS.sky],
        // ['Chest pain type', country.avgCp, COLORS.sky],
        // ['Exercise induced angina', country.avgExang, COLORS.sky],
        // ['Fasting blood sugar', country.avgFbs, COLORS.sky],
        // ['Diagnosis of heart disease', country.avgNum, COLORS.sky],
        // ['ST depression', country.avgOldpeak, COLORS.sky],
        // ['Resting EKG results', country.avgRestecg, COLORS.sky],
        // ['The slope of the peak exercise ST segment ', country.avgSlope, COLORS.sky],
        ['Maximum heart rate achieved', country.avgThalach, COLORS.sky],
        ['Resting blood pressure', country.avgTrestbps, COLORS.sky],
        ['Happiness', getHappiness(country)*50, COLORS.sky]
        ])
    ;

    var options = {
        title: 'Indicating attributes of heart risk',
        height: 170,
        legend: { position: 'none' },
    };

    var chart3 = new google.visualization.BarChart(document.getElementById('BarChart3'));
    chart3.draw(data, options);
}

function getHappiness(country){
    if(country.countryName !== "Global"){
        return country.happiness;
    }

    var sum = 0;
    for(var i=0;i<window.data.length;i++){
        var c = window.data[i];
        sum += c.happiness;
    }

    sum /= (window.data.length - 1);
    return sum;
}

function getPropertiesForCountry(country) {
    var data = window.data;
    for(var i = 0; i < data.length; i++) {
        if(data[i].countryName === country) {
            return data[i];
        }
    }
}