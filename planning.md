Plan of the project:

    1. Task 1: Clean the data and explore it (4h)
        
        - Hristijan Sardjoski (2h)
        
        - Simona Micevska (2h)
    
    2. Task 2: Identify the most relevant attributes using stepwise learning (10h)
    
        - Hristijan Sardjoski (5h)
        
        - Simona Micevska (5h)
    
    3. Task 3: Build several classifiers and choose the one with highest accuracy on the test data. (14h)
    
        - Hristijan Sardjoski (7h)
        
        - Simona Micevska (7h)
    
    4. Task 4: Check whether the country where a person lives in has an impact to his risk for heart disease. (8h)
    
        - Hristijan Sardjoski (4h)
        
        - Simona Micevska (4h)
        
        
    5. Task 5: Create a nice web UI that will explain the project specifications, the business goals etc (8h)
    
        - Hristijan Sardjoski (8h)
        
    6. Task 6: Create visualisations (using d3.js) based on the made discoveries that will be placed in the web ui (8h)
    
        - Simona Micevska (8h)
        
    7. Task 7: Document all findings and prepare the poster for the session (8h)
    
        - Hristijan Sardjoski (4h)
        
        - Simona Micevska (4h)