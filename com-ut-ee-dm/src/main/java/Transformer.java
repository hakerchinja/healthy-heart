import java.io.*;

/**
 *
 * Transforms space separated multiline text files into comma separated single line files
 */
public class Transformer {
    public static void main(String [] args) throws IOException {
        getHeaders("C:\\Users\\Hristijan\\Desktop\\hristijan\\I semestar\\Data Mining" +
                "\\Practical sessions\\HW11\\proba.txt");
        transformFile("C:\\Users\\Hristijan\\Desktop\\hristijan\\I semestar\\Data Mining" +
                "\\Practical sessions\\HW11\\hungarian.data.txt","hungarian.csv");
        transformFile("C:\\Users\\Hristijan\\Desktop\\hristijan\\I semestar\\Data Mining" +
                "\\Practical sessions\\HW11\\cleveland.data.txt","cleveland.csv");
        transformFile("C:\\Users\\Hristijan\\Desktop\\hristijan\\I semestar\\Data Mining" +
                "\\Practical sessions\\HW11\\switzerland.data.txt","switzerland.csv");
        transformFile("C:\\Users\\Hristijan\\Desktop\\hristijan\\I semestar\\Data Mining" +
                "\\Practical sessions\\HW11\\long-beach-va.data.txt","long-beach-va.csv");
    }

    private static void transformFile(String path, String fileName) throws IOException {
        FileReader fileReader = new FileReader(path);

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder currentBuilder = new StringBuilder();

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        int k = 0;

        while((line = bufferedReader.readLine()) != null) {
            String goodLine = line.replaceAll(" ", ",");
            goodLine+=",";
            currentBuilder.append(goodLine);
            if(goodLine.contains("name")){
                currentBuilder.append(goodLine);
                stringBuilder.append(currentBuilder+"\n");
                currentBuilder = new StringBuilder();
            }

        }
        File file = new File(fileName);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(stringBuilder.toString());
        }
    }

    private static void getHeaders(String path) throws IOException {
        FileReader fileReader = new FileReader(path);

        StringBuilder stringBuilder = new StringBuilder();

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        int k = 1;

        while((line = bufferedReader.readLine()) != null) {
            if(line.startsWith(k+"")) {
                k++;
                String [] strings = line.split(" ");
                String attribute = strings[1];
                if(attribute.contains(":")) {
                    attribute = attribute.substring(0, attribute.length()-1);
                }
                stringBuilder.append(attribute+",");
            }
        }
        File file = new File("headers");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(stringBuilder.toString());
        }
    }
}
