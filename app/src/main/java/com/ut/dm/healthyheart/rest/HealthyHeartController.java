package com.ut.dm.healthyheart.rest;

import com.ut.dm.healthyheart.dto.Country;
import com.ut.dm.healthyheart.dto.Person;
import com.ut.dm.healthyheart.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/api")
@CrossOrigin
public class HealthyHeartController {

    @Autowired
    DataService dataService;

    @RequestMapping(value="/countries", method = RequestMethod.GET)
    @ResponseBody
    public List<Country> getCountries(){
        return dataService.getCountries();
    }

    @RequestMapping(value="/people", method=RequestMethod.GET)
    @ResponseBody
    public List<Person> getPeople(){
        return dataService.getPeople();
    }
}
