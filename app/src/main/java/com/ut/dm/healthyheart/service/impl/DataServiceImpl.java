package com.ut.dm.healthyheart.service.impl;

import com.ut.dm.healthyheart.dto.Country;
import com.ut.dm.healthyheart.dto.Person;
import com.ut.dm.healthyheart.service.DataService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataServiceImpl implements DataService {

    private ResourceLoader resourceLoader;

    private List<Country> countries;
    private List<Person> people;

    @Autowired
    public DataServiceImpl(ResourceLoader resourceLoader){
        this.resourceLoader = resourceLoader;
        this.countries = new ArrayList<>();
        this.people = new ArrayList<>();
    }

    @PostConstruct
    public void init(){
        initCountries();
        initPredictedData();
        initTotalRiskPerCountry();
    }

    public void initCountries(){
        BufferedReader br = null;
        String currLine = "";

        try {
            Resource resource = resourceLoader.getResource("classpath:world-happiness-report.csv");
            File file = resource.getFile();

            br = new BufferedReader(new FileReader(file));
            boolean isFirstLine = true;
            while ((currLine = br.readLine()) != null) {
                if(!isFirstLine){
                    currLine = currLine.trim();
                    String[] info = currLine.split(",");
                    Country country = new Country();
                    country.countryName = info[0];
                    country.id = Integer.parseInt(info[1]);
                    country.happiness = Double.parseDouble(info[2]);
                    country.whiskerHigh = Double.parseDouble(info[3]);
                    country.whiskerLow = Double.parseDouble(info[4]);
                    country.economy = Double.parseDouble(info[5]);
                    country.family = Double.parseDouble(info[6]);
                    country.health = Double.parseDouble(info[7]);
                    country.freedom = Double.parseDouble(info[8]);
                    country.generosity = Double.parseDouble(info[9]);
                    country.trustInGovernment = Double.parseDouble(info[10]);
                    country.dystopia = Double.parseDouble(info[11]);
                    country.totalRisk = 0.0;

                    countries.add(country);
                }
                isFirstLine = false;
            }

        } catch (IOException | NullPointerException e) {
        } finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void initPredictedData(){
        BufferedReader br = null;
        String currLine = "";

        try {
            Resource resource = resourceLoader.getResource("classpath:predicted.csv");
            File file = resource.getFile();

            br = new BufferedReader(new FileReader(file));
            boolean isFirstLine = true;
            while ((currLine = br.readLine()) != null) {
                if(!isFirstLine){
                    currLine = currLine.trim();
                    String[] info = currLine.split(",");
                    Person person = new Person();
                    person.id = Integer.parseInt(info[1]);
                    person.age = Integer.parseInt(info[2]);
                    person.sex = Integer.parseInt(info[3]);
                    person.cp = Integer.parseInt(info[4]);
                    person.trestbps = Integer.parseInt(info[5]);
                    person.chol = Integer.parseInt(info[6]);
                    person.fbs = Integer.parseInt(info[7]);
                    person.restecg = Integer.parseInt(info[8]);
                    person.thalach = Integer.parseInt(info[9]);
                    person.exang = Integer.parseInt(info[10]);
                    person.oldpeak = Double.parseDouble(info[11]);
                    person.slope = Integer.parseInt(info[12]);
                    person.num = Integer.parseInt(info[13]);
                    person.country = Integer.parseInt(info[14]);
                    person.happiness = Double.parseDouble(info[15]);
                    person.goal = Double.parseDouble(info[16].replace("\"",""));

                    people.add(person);
                }
                isFirstLine = false;
            }

        } catch (IOException | NullPointerException e) {
        } finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public void initTotalRiskPerCountry(){
        Map<Integer, Double> sum = new HashMap<>();
        Map<Integer, Integer> total = new HashMap<>();

        for(Person person : people){
            if(!sum.containsKey(person.country)){
                sum.put(person.country, 0.0);
                total.put(person.country, 0);
            }

            sum.replace(person.country, sum.get(person.country) + person.goal);
            total.replace(person.country, total.get(person.country) + 1);
        }

        for(Country country : countries){
            country.totalRisk = ((double)sum.get(country.id))/total.get(country.id);

            double sumMale = 0, sumFemale = 0;
            int totalMale = 0, totalFemale = 0;
            double sumAgeGroup1 = 0, sumAgeGroup2 = 0, sumAgeGroup3 = 0, sumAgeGroup4 = 0,
                    sumAgeGroup5 = 0, sumAgeGroup6 = 0;
            int totalAgeGroup1 = 0, totalAgeGroup2 = 0, totalAgeGroup3 = 0, totalAgeGroup4 = 0,
                    totalAgeGroup5 = 0, totalAgeGroup6 = 0;

            double sumCp = 0, sumTrestbps = 0, sumChol = 0, sumFbs = 0,
                    sumRestecg = 0, sumThalach = 0, sumExang = 0,
                    sumOldpeak = 0, sumSlope = 0, sumNum = 0;

            int totalForCountry = 0;

            for(Person person : people){
                if(person.country == country.id){
                    totalForCountry++;

                    sumCp += person.cp;
                    sumTrestbps += person.trestbps;
                    sumChol += person.chol;
                    sumFbs += person.fbs;
                    sumRestecg += person.restecg;
                    sumThalach += person.thalach;
                    sumExang += person.exang;
                    sumOldpeak += person.oldpeak;
                    sumSlope += person.slope;
                    sumNum += person.num;

                    if(person.sex == 0){
                        sumMale += person.goal;
                        totalMale++;
                    } else {
                        sumFemale += person.goal;
                        totalFemale++;
                    }

                    if(person.age < 30){
                        sumAgeGroup1 += person.goal;
                        totalAgeGroup1++;
                    } else if(person.age < 40){
                        sumAgeGroup2 += person.goal;
                        totalAgeGroup2++;
                    } else if(person.age < 50){
                        sumAgeGroup3 += person.goal;
                        totalAgeGroup3++;
                    } else if(person.age < 60){
                        sumAgeGroup4 += person.goal;
                        totalAgeGroup4++;
                    } else if(person.age < 70){
                        sumAgeGroup5 += person.goal;
                        totalAgeGroup5++;
                    } else {
                        sumAgeGroup6 += person.goal;
                        totalAgeGroup6++;
                    }
                }
            }

            country.totalRiskMale = sumMale / totalMale;
            country.totalRiskFemale = sumFemale / totalFemale;
            country.totalRiskAgeGroup1 = sumAgeGroup1 / totalAgeGroup1;
            country.totalRiskAgeGroup2 = sumAgeGroup2 / totalAgeGroup2;
            country.totalRiskAgeGroup3 = sumAgeGroup3 / totalAgeGroup3;
            country.totalRiskAgeGroup4 = sumAgeGroup4 / totalAgeGroup4;
            country.totalRiskAgeGroup5 = sumAgeGroup5 / totalAgeGroup5;
            country.totalRiskAgeGroup6 = sumAgeGroup6 / totalAgeGroup6;
            country.avgCp = sumCp / totalForCountry;
            country.avgTrestbps = sumTrestbps / totalForCountry;
            country.avgChol = sumChol / totalForCountry;
            country.avgFbs = sumFbs / totalForCountry;
            country.avgRestecg = sumRestecg / totalForCountry;
            country.avgThalach = sumThalach / totalForCountry;
            country.avgExang = sumExang / totalForCountry;
            country.avgOldpeak = sumOldpeak / totalForCountry;
            country.avgSlope = sumSlope / totalForCountry;
            country.avgNum = sumNum / totalForCountry;
        }

        //On global level
        Country country = new Country();
        country.id = 0;
        country.countryName = "Global";

        double sumGlobal = 0;
        int totalGlobal = 0;

        double sumMale = 0, sumFemale = 0;
        int totalMale = 0, totalFemale = 0;

        double sumAgeGroup1 = 0, sumAgeGroup2 = 0, sumAgeGroup3 = 0, sumAgeGroup4 = 0,
                sumAgeGroup5 = 0, sumAgeGroup6 = 0;
        int totalAgeGroup1 = 0, totalAgeGroup2 = 0, totalAgeGroup3 = 0, totalAgeGroup4 = 0,
                totalAgeGroup5 = 0, totalAgeGroup6 = 0;

        double sumCp = 0, sumTrestbps = 0, sumChol = 0, sumFbs = 0,
                sumRestecg = 0, sumThalach = 0, sumExang = 0,
                sumOldpeak = 0, sumSlope = 0, sumNum = 0;

        double happiness = 0;

        for(Person person : people){
            sumGlobal += person.goal;
            totalGlobal++;

            sumCp += person.cp;
            sumTrestbps += person.trestbps;
            sumChol += person.chol;
            sumFbs += person.fbs;
            sumRestecg += person.restecg;
            sumThalach += person.thalach;
            sumExang += person.exang;
            sumOldpeak += person.oldpeak;
            sumSlope += person.slope;
            sumNum += person.num;

            happiness += person.happiness;

            if(person.sex == 0){
                sumMale += person.goal;
                totalMale++;
            } else {
                sumFemale += person.goal;
                totalFemale++;
            }

            if(person.age < 30){
                sumAgeGroup1 += person.goal;
                totalAgeGroup1++;
            } else if(person.age < 40){
                sumAgeGroup2 += person.goal;
                totalAgeGroup2++;
            } else if(person.age < 50){
                sumAgeGroup3 += person.goal;
                totalAgeGroup3++;
            } else if(person.age < 60){
                sumAgeGroup4 += person.goal;
                totalAgeGroup4++;
            } else if(person.age < 70){
                sumAgeGroup5 += person.goal;
                totalAgeGroup5++;
            } else {
                sumAgeGroup6 += person.goal;
                totalAgeGroup6++;
            }
        }

        country.totalRisk = sumGlobal / totalGlobal;
        country.totalRiskMale = sumMale / totalMale;
        country.totalRiskFemale = sumFemale / totalFemale;
        country.totalRiskAgeGroup1 = sumAgeGroup1 / totalAgeGroup1;
        country.totalRiskAgeGroup2 = sumAgeGroup2 / totalAgeGroup2;
        country.totalRiskAgeGroup3 = sumAgeGroup3 / totalAgeGroup3;
        country.totalRiskAgeGroup4 = sumAgeGroup4 / totalAgeGroup4;
        country.totalRiskAgeGroup5 = sumAgeGroup5 / totalAgeGroup5;
        country.totalRiskAgeGroup6 = sumAgeGroup6 / totalAgeGroup6;

        country.avgCp = sumCp / totalGlobal;
        country.avgTrestbps = sumTrestbps / totalGlobal;
        country.avgChol = sumChol / totalGlobal;
        country.avgFbs = sumFbs / totalGlobal;
        country.avgRestecg = sumRestecg / totalGlobal;
        country.avgThalach = sumThalach / totalGlobal;
        country.avgExang = sumExang / totalGlobal;
        country.avgOldpeak = sumOldpeak / totalGlobal;
        country.avgSlope = sumSlope / totalGlobal;
        country.avgNum = sumNum / totalGlobal;
        country.happiness = happiness / totalGlobal;

        countries.add(country);
    }

    @Override
    public List<Country> getCountries() {
        return countries;
    }

    @Override
    public List<Person> getPeople() {
        return people;
    }
}
