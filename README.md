# README #

1. Business understanding: https://bitbucket.org/hakerchinja/healthy-heart/src/6a1bcbb44cba3ef45a9fe5de06269ff56a7752ae/business-understanding.md?at=master&fileviewer=file-view-default

2. Data understanding: https://bitbucket.org/hakerchinja/healthy-heart/src/9c93c8308490a7898d9ea84b57c7da13dfa39838/data-understanding.md?at=development&fileviewer=file-view-default

3. Planning: https://bitbucket.org/hakerchinja/healthy-heart/src/cd891d35130bc56925a5f9f3ba24fab549183720/planning.md?at=master&fileviewer=file-view-default


Registration for the project: https://docs.google.com/presentation/d/1veA_WQcfRRx7hQnE8qklmsLYceWzQGrPHrieSEWqcaI/edit#slide=id.g2a57963548_147_0


# Healthy heart
The goal of the project is to benefit the general public based on publicly available data for patients that either have or don't have heart disease. The core of the application is the constructed model which can predict whether a person is at risk for having a heart disease based on given properties. The used data is from different patients, from different hospitals around the world. This results are presented in a fine manner using nice visualizations.

# Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

First you need a git tool.

	git https://hristijan_sardzoski@bitbucket.org/hakerchinja/healthy-heart.git

Second, you need a tomcat server for running on local machine for development and testing services, possibly using :8080 or :8000 ports.
 
# Prerequisites
What things you need to install the software and how to install them.

For windows users, whether is 32bit or 64 bit machine.

	R studio with R i386 3.4.2
    R x64 3.4.2 
    
    Eclipse IDE or IntelliJ IDEA(preffereable)
    Maven plugin
    Tomcat server embedded in Eclipse or Intellij
    Spring boot
    Java JDK 8.0 or higher
    
For installing R 32bit or R 64bit use the following link:
		
   	https://cran.r-project.org/bin/windows/base/
    
For installing Intellij IDEA use the following link. This comes with embedded tomcat server, maven plugin, and opportunity to build Spring Boot application.
 	
    https://www.jetbrains.com/idea/
 
For installing Eclipse IDE and using Tomcat, Maven, Spring Boot you need to download and manually set up all of them.
	
For manually downloading tomcat server:
https://www.eclipse.org/webtools/jst/components/ws/M5/tutorials/InstallTomcat.html

For installing Java JDK use the following link(9.0 preferable):
	
	http://www.oracle.com/technetwork/java/javase/downloads/index.html
    
# Running the application(s)
After the setup of the required utilities, next is to explain how to start and use the application(s).

The application is divided into three parts(applications).
	
    R application with the Data part
    Web application
    Java helpper application
 
    
The R application is under /rscripts. For using the R scripts the R studio is required. All that needs to be done here is to open the /rscripts folder under new project in the R studio and import the scripts. These scripts are used for building classifiers which can be further used for predicting the risk of a possible heart disease. Alongside the classifiers, there are the scripts for simple linear regression and multi-variate linear regression which were used for testing the class probabilities and inferring which attributes of the initial data set are actually needed in order to score nice results while predicting heart risks. In order to try it out, or do some changes the only thing here that needs to be taken care of is the correct path of the file that it is read. 

The data is under /data and has four parts:
	
    - The combined heart disease data from the hospitals
    - The world happiness data for each country
    - The classifier outputs which were needed to examine which classifiers should be further used
    - The regression outputs which were needed to examine what class probability values are recevied based on the examined classifiers and which attributes fit the most for predicting

The Web application is under /app. For using the web application Java JDK 8.0 or higher is required, alongside Tomcat Server. The application can be run as Spring Boot application. In this part, if using IntelliJ IDEA, one should import the /app web application and run the pom.xml file to get the required dependencies. After importing the project the setup of Application should take place. If for some reason, Intellij does not setup the application runner by itself(which will be very strange), Edit Configuration under Run section should be opened. There, first we add new configuration on the green plus sign under Spring Boot. Afterwards on the right side of the panel we should specify the name of the runner of the spring boot application, and add the main class which is the HealthyHeartApplication.java with the @SpringBootApplication annotation. Afterwards, the classpath of the module should be set to healthy-heart and last but not the least the JRE path should be set as well. Make sure that in the pom.xml file you have the following dependency:

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

After this, the only thing left is to check wheter port :8080 is currently taken or not. If it is, make sure to free this port. At last, we can run the application and we should look it up under
	
    localhost:8080

If you receive the following using this url, than you have successfully set up the web application.

![Web Application](/screenshots/app-image.png)

The Java helpper application is under /com-ut-ee-dm. And the process of running this part of the application is similar to the previous, but there is no need of servers and ports. This application is used for two general purposes and consists two classes.
	
    TestDataGenerator class
    Transformer class
    
The first class is used for generation of random persons, with random attributes in order to use the selected classifier and predict the heart risk.

The second class transforms space separated multiline text files into comma separated single line files and extract their headers. This was due to bad formatting of the hospital records from where the data comes from.
 
# R scripts 

The R scripts were used for the data mining part of this application and can be found under /rscripts. The scripts are separated into three groups.
	
    Classifier scripts 
    Regression scripts
    Tryout scripts
   
They produce data under /data.
	
    Classifier outputs
    Regression outputs
    
The classifier scripts implement various classifiers such as:
	
    KNN classifier
    C5.0 classifier
    Radial classifier
    Linear classifier
    Random Forest classifier
    XGBoost classifier
    
In these scripts the classifiers are trained on the given data from the hospitals which was combined with the data of the world happiness report. In order to check there correctness cross validation was performed. The best classifier was c5.0 according to the outputs.
These scripts also implement oversampling on the data, and again, the same classifiers are used. The correctness of the classifiers is documented under /data/classifier outputs, where it can be seen that each classifier was tested with cross-validation 100 times, and for each classifier there is corresponding output.
 
 
The regression scripts use:
	
    Linear regression
    Stepwise multi variate regression with forward and backward models
    Class probability 
    
After using the stepwise multi variate regression it was concluded that some parameters have bigger impact than others. Afterwards, those parameters were used in the linear regression to see what probabilities are going to be gained for the different possible output labels. Basically, those parameters were used in the classifiers as well. The outputs of the regression testing are documented under /data/regression outputs and there are different outputs for the forward and the backward model using the stepwise multi variate regression, as well as the output of the linear regression.
    
The try out scripts are:

	1. script-c50-oversample.R
    2. script-both.R
    3. script-h.R
    4. script-xgboost.R
    
1. The first script is for oversampling. The goal of the application is to build an application which is able to give us an overview of the heart risks per country based on given parameters. The input data marks this risks on the scale from 0 to 4. But the data was heavily imbalanced. That is why the first tryout was to oversample the data in order to receive more precise results. Here, the following was tried. First filter the data, and create sub-data with only two goal values, or in other words divide the data into four subgroups, where the label for predicting is in the form of two values out of the possible five(ex. 1 and 0, 1 and 2, 1 and 3, 1 and 4). We took 1 to be present in all of these subgroups because we found out that the data is heavily imbalanced due to the value of 1, and the classifiers were always in favor of 1. After the oversampling we combined those subgroups, but the documented classifier showed low accuracy, always around 30%.
2.  The second script is oversampling and undersampling (both). In this case, we translate 0 and 1 to 0 and 2,3 and 4 to 1. Next we tried with "both" and trained c5.0 classifier, and got better results, somewhere above 60%.
3. In the third script we started cleaning the data from bad numeric, character and factor attributes. For that the following was used scheme:
		
        Find the number of levels of factor variables, if this is one than it is dropped.
        Find the number of levels of character variables, if this is one than it is dropped.
        Find the number of levels of numeric variables, if this is one than it is dropped.
        Get rid of columns where for ALL rows have NA's
        Extract factor columns and drop redundant levels
	After this was done, the data was able to be processed for stepwise multivariate 	regression, as well as for over and under sampling.  

4. The fourth script implements XGBoost, where first the documentation is necessary to be read in order to have enough knowledge to start using this classifier.
		
        https://cran.r-project.org/web/packages/xgboost/xgboost.pdf
        
    We came across this algorithm from more experienced data scientist and kaggle who suggested that this is popular winning recipe of data science. Unfortunately, after we implemented it, we did not receive something really good. I guess, our implementation skills in R are yet to be developed. XGBoost has both linear model solver and tree learning algorithms. It has very high predictive power , and also has additional features for doing cross validation and finding important variables. There are many parameters which needs to be controlled to optimize the model. First the data was transformed into sparse.model.matrix, because this will be need for testing at the end. After that the dataset is loaded, and the labels from the train dataset are extracted. After that, the train and the test datasets are combined. Next step is data cleaning and feature engineering, where one hot encoding categorical features need to be derived. The last step before using the algorithm is to tune the model. Here we tune many different parameters. And at last we compute the score of the test population.

# Web application
The web application is divided into two parts:
		
    Java backend calculations perfomred on the generated data using R
    Java script frontend visualization of the processed data
 
 
The Web application is under /app. For using the web application Java JDK 8.0 or higher is required.

1. The Java backend calculations are performed using Spring Boot. The class seperation is the following:
	
    	-dto
   	 	-rest controllers
    	-services and their implementations
    	-the core spring boot application
    
    The dtos represent the objects that can be inferred from the input data, and these are Person and Country. They have the attributes which correspond to the generated data from the R scripts. The data is under the part /resources in the files predicted.csv and world-happiness-report.csv.

    The rest controllers folder consist the HealthyHeartController where the data is mapped for java script visualization using google charts under the urls:
	
   	 	/api/countries
     	/api/people 
    
    The services folder consists the DataService, where the list of countries and the resulting risk factors are formatted for better visualization. Here the global report is generated, and the report per country is generated.

    The core spring boot application is used for running the application on server(in our case on local machine).
    
2. The Java script frontend visualization is the following:
 	
     	-connectApi
     	-javascript loader
    	-main javascript logic for visualization
    
    In the connectApi the connection between Java Healthy heart controller and the main.js is established. Here we hit the two urls (/countries and /people) with ajax get callbacks.
    
    In the javascript loader the necessary javascripts are loaded. These are the following:
    	
        -js/connectApi.js
        -js/main.js
        
     In the main.js the visualization of generated R data takes place. This part is separated in three vital subparts. 
     	
        -world map
        -global statistics or statistics per country
        -global statistics for Happiness vs. Total risk of heart disease
        
    The world map represents google visualization geochart on which most of the countries of the world are presented with their average total risk of heart diseases.
    The initial code for the geochart is the following:
    
    	var chartData = google.visualization.arrayToDataTable(data);
        var options = {colorAxis: {colors: ['#EFEFEF', '#FF3B3F']}};

        var chart = new google.visualization.GeoChart(document.getElementById('container'));
        chart.draw(chartData, options);
    
 	The global statistics or statistics per country are shown on three additional google charts:
    	
        -Country Male/Female overall risk factor
        -Country Age groups risk factor
        -Country Indicating attributes of heart risk
        
     All of these are google visualization bar charts. The first one represents the heart risks based on the gender. The second one represents the heart risks based on the age group, and the third chart represents the direct impact on the increased heart risks. The following code is for implementing the google visualization bar charts.
     
     	    google.charts.load("current", {packages:["corechart"], 'callback': function(){ drawCharts(country) }});


     	var data = google.visualization.arrayToDataTable([
        ['Gender', 'Risk', { role: 'style' }],
        ['Male', country.totalRiskMale*100, COLORS.sky],
        ['Female', country.totalRiskFemale*100, COLORS.watermelon]
    	]);

    	var options = {
        title: country.countryName + " Male/Female overall risk factor",
        width: 430,
        height: 170,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    	}

    	var view = new google.visualization.DataView(data);
    	view.setColumns([0, 1,
        { calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation" },
        2]);


    	var chart = new google.visualization.BarChart(document.getElementById("BarChart1"));
    	chart.draw(view, options);
        
     The Happiness vs. Total risk of heart disease represents google visualization ScatterChart on which the x axis is the distributed hapness and on the y axis the total risk of heart disease.
     The following code was used:
         
           var data = google.visualization.arrayToDataTable(getData());

           var options = {
               title: 'Happiness vs. Total risk of heart disease',
               hAxis: {title: 'Happiness', minValue: 0},
               vAxis: {title: 'Total risk', minValue: 0},
               legend: 'none'
           };

           var chart = new google.visualization.ScatterChart(document.getElementById('scatterChart'));

           chart.draw(data, options);
            
# Built With
R

Java - Spring Boot

JavaScript - Google Charts

Maven - Dependency Management

# Authors 
Simona Micevska 

Hristijan Sardjoski

# Aknowledments

Student Project

Data mining 

Inspiration



 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
    