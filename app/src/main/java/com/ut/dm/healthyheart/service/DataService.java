package com.ut.dm.healthyheart.service;

import com.ut.dm.healthyheart.dto.Country;
import com.ut.dm.healthyheart.dto.Person;

import java.util.List;

public interface DataService {

    List<Country> getCountries();

    List<Person> getPeople();
}
