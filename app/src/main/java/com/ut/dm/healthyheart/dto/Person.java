package com.ut.dm.healthyheart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Person {
    @JsonProperty("id")
    public int id;

    @JsonProperty("age")
    public int age;

    @JsonProperty("sex")
    public int sex;

    @JsonProperty("cp")
    public int cp;

    @JsonProperty("trestbps")
    public int trestbps;

    @JsonProperty("chol")
    public int chol;

    @JsonProperty("fbs")
    public int fbs;

    @JsonProperty("restecg")
    public int restecg;

    @JsonProperty("thalach")
    public int thalach;

    @JsonProperty("exang")
    public int exang;

    @JsonProperty("oldpeak")
    public double oldpeak;

    @JsonProperty("slope")
    public int slope;

    @JsonProperty("num")
    public int num;

    @JsonProperty("country")
    public int country;

    @JsonProperty("happiness")
    public double happiness;

    @JsonProperty("goal")
    public double goal;
}
